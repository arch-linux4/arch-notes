# Arch Linux Server Guide

## Security setup

Firewall:
```sh
sudo pacman -S iptables ufw
```

Malware scanning:
```sh
sudo pacman -S clamav
```

Log scanner/IP banner:
```sh
sudo pacman -S fail2ban
```

Mandatory access control:
```sh
sudo pacman -S apparmor
```

## Backup setup
Semi-professional solution:
```sh
sudo pacman -S borg
```

