# Development

## Flash ROM chips
```bash
sudo pacman -S flashrom
```

## C/C++
```bash
sudo pacman -S gcc
```

## Java

Install the latest Java Development Kit:
```bash
sudo pacman -S jdk-openjdk
```

Set default system-wide java version:
```bash
sudo archlinux-java set java-20-openjdk # for java 20
```

Check installed java versions:
```bash
archlinux-java status
```

## Python
```bash
?
```

## Lua
```bash
sudo pacman -S lua
```

## LaTex
```bash
sudo pacman -S texlive-most texlive-lang texstudio
```

## HTML/CSS/Javascript/PHP
```bash
?
```

## Arduino
```bash
paru -S arduino-ide-bin
```

## OpenSCAD
```bash
sudo pacman -S openscad
```

## R
```bash
sudo pacman -S r gcc-fortran
paru -S rstudio-desktop-bin
```

## PostgreSQL

Install the following:
```bash
sudo pacman -S postgresql
sudo dbeaver
```

Setup the database, follow the instructions at:
https://wiki.archlinux.org/title/PostgreSQL

## TTF/OTF fonts dev
```bash
sudo pacman -S birdfont gucharmap fontforge
```

## Minecraft Mods
```bash
sudo pacman -S jdk8-openjdk jdk-openjdk
paru -S blockbench-bin
```

## Minetest
```bash
sudo pacman -S minetest
```

## Godot
```bash
sudo pacman -S godot
paru -S godot3-bin godot-voxel
```

## Matlab
```bash
paru -S matlab
```

## Arch packages
```bash
sudo pacman -S dev-tools namcap
```

## Static websites
Hugo:
```bash
sudo pacman -S go hugo
```

Jekyll:
```bash
sudo pacman -S ruby rubygems gcc make
gem install jekyll bundler
```

Sphynx:
```bash
?
```

## Chemistry - Molecule Modeling
```bash
paru -S avogadro2
```
